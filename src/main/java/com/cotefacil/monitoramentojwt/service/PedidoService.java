package com.cotefacil.monitoramentojwt.service;

import com.cotefacil.monitoramentojwt.domain.Cliente;
import com.cotefacil.monitoramentojwt.domain.Comprador;
import com.cotefacil.monitoramentojwt.domain.Pedido;
import com.cotefacil.monitoramentojwt.dto.ClienteDTO;
import com.cotefacil.monitoramentojwt.dto.CompradorDTO;
import com.cotefacil.monitoramentojwt.dto.DadosClienteDTO;
import com.cotefacil.monitoramentojwt.dto.PedidoDTO;
import com.cotefacil.monitoramentojwt.repository.ClienteRepository;
import com.cotefacil.monitoramentojwt.repository.CompradorRepository;
import com.cotefacil.monitoramentojwt.repository.PedidoRepository;
import com.cotefacil.monitoramentojwt.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class PedidoService {

    public static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
    @Autowired
    private PedidoRepository pedidoRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private CompradorRepository compradorRepository;

    @Transactional(readOnly = true)
    public Page<PedidoDTO> findPedidos(String minDate, String maxDate, Long idPedido, Long idCotacao, String fornecedor, Pageable pageable) {
        LocalDateTime today = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
        LocalDateTime min = minDate.equals("") ? today.minus(5, ChronoUnit.YEARS) : LocalDateTime.parse(minDate + " 00:00:00", FORMATTER);
        LocalDateTime max = maxDate.equals("") ? today : LocalDateTime.parse(maxDate + " 00:00:00", FORMATTER);

        Page<Pedido> page = pedidoRepository.findPedidos(min, max, idPedido, idCotacao, fornecedor, pageable);
        return page.map(PedidoDTO::new);
    }
    @Transactional(readOnly = true)
    public DadosClienteDTO findByIdPedido(Long id) {
        Optional<Pedido> obj = pedidoRepository.findById(id);
        Pedido entity = obj.orElseThrow(() -> new ResourceNotFoundException("Id não encontrado: " + id));
        return new DadosClienteDTO(entity);
    }
    @Transactional(readOnly = true)
    public ClienteDTO findByIdCliente(Long id) {
        Optional<Cliente> obj = clienteRepository.findById(id);
        Cliente entity = obj.orElseThrow(() -> new ResourceNotFoundException("Id não encontrado: " + id));
        return new ClienteDTO(entity, entity.getContatos());
    }
    @Transactional(readOnly = true)
    public CompradorDTO findByIdComprador(Long id) {
        Optional<Comprador> obj = compradorRepository.findById(id);
        Comprador entity = obj.orElseThrow(() -> new ResourceNotFoundException("Id não encontrado: " + id));
        return new CompradorDTO(entity);
    }
}
