package com.cotefacil.monitoramentojwt.service;

import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import com.cotefacil.monitoramentojwt.domain.UsuarioMonitoramento;
import com.cotefacil.monitoramentojwt.dto.RoleMonitoramentoDTO;
import com.cotefacil.monitoramentojwt.dto.UsuarioMonitoramentoDTO;
import com.cotefacil.monitoramentojwt.repository.RoleMonitoramentoRepository;
import com.cotefacil.monitoramentojwt.repository.UsuarioMonitoramentoRepository;
import com.cotefacil.monitoramentojwt.service.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UsuarioMonitoramentoServiceImpl implements UsuarioMonitoramentoService, UserDetailsService {

    @Autowired
    private final UsuarioMonitoramentoRepository usuarioRepository;
    private final RoleMonitoramentoRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException {
        UsuarioMonitoramento usuarioMonitoramento = usuarioRepository.findByUsuarioIgnoreCase(usuario);
        if(usuarioMonitoramento == null){
            log.error("Usuário não encontrado no banco de dados");
            throw new UsernameNotFoundException("Usuário não encontrado no banco de dados");
        }
        else{
            log.info("Usuário encontrado no banco de dados: {}", usuario);
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        usuarioMonitoramento.getRoles().forEach(role ->{
            authorities.add(new SimpleGrantedAuthority(role.getNome()));
        });
        return new User(usuarioMonitoramento.getUsuario(), usuarioMonitoramento.getPassword(), authorities);
    }

   /* @Override
    public UsuarioMonitoramento saveUsuario(UsuarioMonitoramento usuario) {
        log.info("Salvando novo usuário {} para o banco de dados", usuario.getUsuario());
        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
        return usuarioRepository.save(usuario);
    }

    @Override
    public RoleMonitoramento saveRole(RoleMonitoramento role) {
        log.info("Salvando nova função(role) {} para o banco de dados", role.getNome());
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUse(String usuario, String roleNome) {
        log.info("Adicionando função {} para usuário {}", usuario, roleNome);
        UsuarioMonitoramento usuarioMonitoramento = usuarioRepository.findByUsuarioIgnoreCase(usuario);
        RoleMonitoramento roleMonitoramento = roleRepository.findByNome(roleNome);
        usuarioMonitoramento.getRoles().add(roleMonitoramento);
    }*/

    @Override
    public UsuarioMonitoramento getUsuario(String usuario) {
        log.info("Buscando usuário {}", usuario);
        return usuarioRepository.findByUsuarioIgnoreCase(usuario);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UsuarioMonitoramentoDTO> findAllUsuariosAtivos(Pageable pageable) {
        log.info("Buscando todos usuários");
        Page<UsuarioMonitoramento> page = usuarioRepository.findUsuariosAtivos(pageable);
        return page.map(UsuarioMonitoramentoDTO::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UsuarioMonitoramentoDTO> findAll(Pageable pageable) {
        Page<UsuarioMonitoramento> page = usuarioRepository.findAll(pageable);
        return page.map(UsuarioMonitoramentoDTO::new);
    }

    @Override
    @Transactional(readOnly = true)
    public UsuarioMonitoramentoDTO findById(Long id) {
        Optional<UsuarioMonitoramento> obj = usuarioRepository.findById(id);
        UsuarioMonitoramento entity = obj.orElseThrow(() -> new ResourceNotFoundException("Id não encontrado: " + id));
        return new UsuarioMonitoramentoDTO(entity, entity.getRoles());
    }

    @Override
    public UsuarioMonitoramentoDTO saveUser(UsuarioMonitoramentoDTO dto) {
        log.info("Salvando usuario {} para o banco de dados", dto.getUsuario());
        UsuarioMonitoramento entity = new UsuarioMonitoramento();
        copyDtoToEntity(dto, entity);
        entity = usuarioRepository.save(entity);
        return new UsuarioMonitoramentoDTO(entity);
    }

    @Override
    public void deleteById(Long id, UsuarioMonitoramentoDTO dto) {
        try{
            UsuarioMonitoramento entity = usuarioRepository.getOne(id);
            if(entity.getHabilitado()) {
                entity.setHabilitado(!dto.getHabilitado());
                entity = usuarioRepository.save(entity);
            }
            else{
                throw new ResourceNotFoundException("Usuário inativo");
            }
        }catch(EntityNotFoundException e){
            throw new ResourceNotFoundException("Id não encontrado: " + id);
        }

    }

    private void copyDtoToEntity(UsuarioMonitoramentoDTO dto, UsuarioMonitoramento entity) {
        entity.setUsuario(dto.getUsuario());
        entity.setPassword(passwordEncoder.encode(dto.getPassword()));
        entity.setDataCadastro(new Date(System.currentTimeMillis()));
        entity.setHabilitado(dto.getHabilitado());
        entity.getRoles().clear();

        for(RoleMonitoramentoDTO roleDTO : dto.getRoles()) {
            RoleMonitoramento role = roleRepository.getOne(roleDTO.getId());
            entity.getRoles().add(role);
        }
    }
}
