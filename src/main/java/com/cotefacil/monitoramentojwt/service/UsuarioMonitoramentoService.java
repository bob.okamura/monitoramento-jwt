package com.cotefacil.monitoramentojwt.service;

import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import com.cotefacil.monitoramentojwt.domain.UsuarioMonitoramento;
import com.cotefacil.monitoramentojwt.dto.UsuarioMonitoramentoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UsuarioMonitoramentoService {

//    UsuarioMonitoramento saveUsuario(UsuarioMonitoramento usuario);
//    RoleMonitoramento saveRole(RoleMonitoramento role);
//    void addRoleToUse(String usuario, String roleNome);
    UsuarioMonitoramento getUsuario(String usuario);
    UsuarioMonitoramentoDTO saveUser(UsuarioMonitoramentoDTO dto);
    Page<UsuarioMonitoramentoDTO> findAllUsuariosAtivos(Pageable pageable);
    Page<UsuarioMonitoramentoDTO> findAll(Pageable pageable);
    UsuarioMonitoramentoDTO findById(Long id);
    void deleteById(Long id, UsuarioMonitoramentoDTO dto);


}
