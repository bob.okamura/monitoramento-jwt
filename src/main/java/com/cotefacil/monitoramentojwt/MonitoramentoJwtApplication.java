package com.cotefacil.monitoramentojwt;

import com.cotefacil.monitoramentojwt.config.PasswordEncoderSHA256Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class MonitoramentoJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoramentoJwtApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new PasswordEncoderSHA256Base64();
	}

}
