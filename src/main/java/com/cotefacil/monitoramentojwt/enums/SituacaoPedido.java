package com.cotefacil.monitoramentojwt.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SituacaoPedido {

    /**
     * Situação do pedido de índice 0 - Não Iniciado - Índice por ordem de
     * negócio - 1
     */
    NAO_INICIADO("Não iniciado", 1),
    /**
     * Situação do pedido de índice 1 - Em criação - Índice por ordem de negócio
     * - 2
     */
    EM_CRIACAO("Em criação", 2),
    /**
     * Situação do pedido de índice 2 - Confirmado - Índice por ordem de negócio
     * - 4
     */
    CONFIRMADO("Confirmado", 4),
    /**
     * Situação do pedido de índice 3 - Enviado - Índice por ordem de negócio -
     * 6
     */
    ENVIADO("Enviado", 6),
    /**
     * Situação do pedido de índice 4 - Aguardando Faturamento - Índice por
     * ordem de negócio - 7
     */
    AGUARDANDO_FATURAMENTO("Enviado - Sem Retorno", 7),
    /**
     * Situação do pedido de índice 5 - Faturado Manualmente - Índice por ordem
     * de negócio - 8 <br>
     * Utilizado quando e faturado pelo representante.
     */
    FATURADO_MANUALMENTE("Confirmado pelo representante", 8),
    /**
     * Situação do pedido de índice 6 - Faturado - Retorno com sucesso - Índice
     * por ordem de negócio - 9 <br>
     * Utilizado quando e faturado pela integracao.
     */
    FATURADO_RETORNO("Faturado", 9),
    /**
     * Situação do pedido de índice 7 - Cancelado - Índice por ordem de negócio
     * - 0
     */
    CANCELADO("Cancelado", 0),
    /**
     * Situação do pedido de índice 8 - Em Negociação - Índice por ordem de
     * negócio - 3
     */
    EM_NEGOCIACAO("Em negociação", 3),
    /**
     * Situação do pedido de índice 9 - Enviado, mas com problema (quando ocorre
     * erro na integração) - Índice por ordem de negócio - 5
     */
    ENVIADO_PROBLEMA("Problema no envio", 5),
    /**
     * Situação do pedido de índice 10 - Enviado drogaria São Paulo
     */
    ENVIADO_FTP("Aguardando código de pedido", 10),
    /**
     * Situação do pedido de índice 11 - Não Faturado - Índice por ordem de
     * negócio - 11
     */
    NAO_FATURADO("Não Faturado", 11),
    /**
     * Situação do pedido de índice 12 - Faturado Parcialmente - Retorno com
     * Sucesso - Índice por ordem de negócio - 12 <br>
     * Utilizado quando o faturamento é incompleto pela integracao .
     */
    FATURADO_RETORNO_INCOMPLETO("Faturado Parcialmente", 12);

    private String nome;
    private int indiceNegocio;

    @Override
    public String toString() {
        return this.nome;
    }

}
