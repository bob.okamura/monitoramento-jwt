package com.cotefacil.monitoramentojwt.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SituacaoCotacao {

    /**
     * Situação da cotação de índice 0 - Em Criação
     */
    EM_CRIACAO("Em Criação"),
    /**
     * Situação da cotação de índice 1 - Em Andamento<br>
     * Anteriormente era AGUARDANDO_RESPOSTA("Aguardando Respostas")
     */
    EM_ANDAMENTO("Em Andamento"),
    /**
     * Situação da cotação de índice 2 - Em Análise<br>
     * Anteriormente era ENCERRADA("Encerrada")
     */
    EM_ANALISE("Em Analise"),
    /**
     * Situação da cotação de índice 3 - Cancelada
     */
    CANCELADA("Cancelada"),
    /**
     * Situação da cotação de índice 4 - Enviada para Matriz
     */
    ENVIADA_MATRIZ("Enviada para Matriz"),
    /**
     * Situação da cotação de índice 5 - Associada pela Matriz<br>
     * Anteriormente era COTACAO_ASSOCIAR("Cotação para Associar")
     */
    ASSOCIADA_MATRIZ("Associada pela Matriz"),
    /**
     * Situação da cotação de índice 6 - Aguardando Processamento
     */
    AGUARDANDO_PROCESSAMENTO("Aguardando Processamento"),
    /**
     * Situação da cotação de índice 7 - Em Processamento
     */
    EM_PROCESSAMENTO("Em Processamento"),
    /**
     * Situação da cotação de índice 8 - Capturada
     */
    CAPTURADA("Capturada"),
    /**
     * Situação da cotação de índice 9 - Finalizada
     */
    FINALIZADA("Finalizada"),
    /**
     * Situação da cotação de índice 10 - Teste
     */
    FAKE("Teste");

    private String nome;

    @Override
    public String toString() {
        return this.nome;
    }

}
