package com.cotefacil.monitoramentojwt.repository;

import com.cotefacil.monitoramentojwt.domain.Comprador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompradorRepository extends JpaRepository<Comprador, Long> {
}
