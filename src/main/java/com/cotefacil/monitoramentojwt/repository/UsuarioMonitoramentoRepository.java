package com.cotefacil.monitoramentojwt.repository;

import com.cotefacil.monitoramentojwt.domain.UsuarioMonitoramento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioMonitoramentoRepository extends JpaRepository<UsuarioMonitoramento, Long> {

    UsuarioMonitoramento findByUsuarioIgnoreCase(String usuario);
    @Query(value = "SELECT u FROM UsuarioMonitoramento u WHERE u.habilitado = true")
    Page<UsuarioMonitoramento> findUsuariosAtivos(Pageable pageable);
}
