package com.cotefacil.monitoramentojwt.repository;

import com.cotefacil.monitoramentojwt.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
