package com.cotefacil.monitoramentojwt.repository;

import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleMonitoramentoRepository extends JpaRepository<RoleMonitoramento, Long> {

     RoleMonitoramento findByNome(String nome);
}
