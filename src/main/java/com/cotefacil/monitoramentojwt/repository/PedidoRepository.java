package com.cotefacil.monitoramentojwt.repository;

import com.cotefacil.monitoramentojwt.domain.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    @Query("SELECT p FROM Pedido p WHERE (:idPedido IS NULL OR :idPedido = p.id) " +
            "AND (:idCotacao IS NULL OR :idCotacao = p.cotacao.id) " +
            "AND (:fornecedor IS NULL OR UPPER(p.fornecedor.nomeFantasia) LIKE UPPER(CONCAT(:fornecedor, '%'))) " +
            "AND (:min IS NULL OR p.dataCadastro >= :min) " +
            "AND (:max IS NULL OR p.dataCadastro <= :max) ")
    Page<Pedido> findPedidos(LocalDateTime min, LocalDateTime max, Long idPedido, Long idCotacao, String fornecedor, Pageable pageable);
}
