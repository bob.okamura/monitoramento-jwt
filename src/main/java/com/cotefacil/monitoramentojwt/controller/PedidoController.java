package com.cotefacil.monitoramentojwt.controller;

import com.cotefacil.monitoramentojwt.dto.ClienteDTO;
import com.cotefacil.monitoramentojwt.dto.CompradorDTO;
import com.cotefacil.monitoramentojwt.dto.DadosClienteDTO;
import com.cotefacil.monitoramentojwt.dto.PedidoDTO;
import com.cotefacil.monitoramentojwt.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    @Autowired
    private PedidoService service;

    @GetMapping("/buscarpedidos")
    public ResponseEntity<Page<PedidoDTO>> findPedidos(
            @RequestParam(value = "minDate", defaultValue = "")String minDate,
            @RequestParam(value = "maxDate", defaultValue = "")String maxDate,
            @RequestParam(value = "idPedido", defaultValue = "")Long idPedido,
            @RequestParam(value = "idCotacao", defaultValue = "")Long idCotacao,
            @RequestParam(value = "fornecedor" , defaultValue = "")String fornecedor,
            Pageable pageable){
        Page<PedidoDTO> page = service.findPedidos(minDate, maxDate, idPedido, idCotacao, fornecedor, pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/dadoscliente/idpedido/{id}")
    public ResponseEntity<DadosClienteDTO> findByIdPedido(@PathVariable Long id) {
        DadosClienteDTO dto = service.findByIdPedido(id);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/dadoscliente/idcliente/{id}")
    public ResponseEntity<ClienteDTO> findByIdCliente(@PathVariable Long id) {
        ClienteDTO dto = service.findByIdCliente(id);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/dadoscliente/idcomprador/{id}")
    public ResponseEntity<CompradorDTO> findByIdComprador(@PathVariable Long id) {
        CompradorDTO dto = service.findByIdComprador(id);
        return ResponseEntity.ok().body(dto);
    }
}
