package com.cotefacil.monitoramentojwt.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import com.cotefacil.monitoramentojwt.domain.UsuarioMonitoramento;
import com.cotefacil.monitoramentojwt.dto.UsuarioMonitoramentoDTO;
import com.cotefacil.monitoramentojwt.form.RoleToUserForm;
import com.cotefacil.monitoramentojwt.service.UsuarioMonitoramentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UsuarioMonitoramentoController {

    private final UsuarioMonitoramentoService usuarioService;

    @GetMapping("/usuariosativos")
    public ResponseEntity<Page<UsuarioMonitoramentoDTO>> findAllUsuariosAtivos(Pageable pageable){
        return ResponseEntity.ok().body(usuarioService.findAllUsuariosAtivos(pageable));
    }

    @GetMapping("/usuarios")
    public ResponseEntity<Page<UsuarioMonitoramentoDTO>> findAllUsuarios(Pageable pageable){
        return ResponseEntity.ok().body(usuarioService.findAll(pageable));
    }

    @GetMapping("/buscaporid/{id}")
    public ResponseEntity<UsuarioMonitoramentoDTO> findById(@PathVariable Long id){
        return ResponseEntity.ok().body(usuarioService.findById(id));
    }

    @PostMapping("/usuario/salvar")
    public ResponseEntity<UsuarioMonitoramentoDTO> salvar(@RequestBody UsuarioMonitoramentoDTO dto){
        dto = usuarioService.saveUser(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id, UsuarioMonitoramentoDTO dto){
        usuarioService.deleteById(id, dto);
        return ResponseEntity.noContent().build();
    }

 /*   @PostMapping()
    public ResponseEntity<UsuarioMonitoramento> saveUsuario(@RequestBody UsuarioMonitoramento usuario){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/").toUriString());
        return ResponseEntity.created(uri).body(usuarioService.saveUsuario(usuario));
    }

    @PostMapping("/role/salvar")
    public ResponseEntity<RoleMonitoramento> saveRole(@RequestBody RoleMonitoramento role){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/salvar").toUriString());
        return ResponseEntity.created(uri).body(usuarioService.saveRole(role));
    }

    @PostMapping("/role/addtouser")
    public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form){
        usuarioService.addRoleToUse(form.getUsuario(), form.getRolename());
        return ResponseEntity.ok().build();
    }*/

    @GetMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try{
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token);
                String usuario = decodedJWT.getSubject();
                UsuarioMonitoramento usuarioMonitoramento = usuarioService.getUsuario(usuario);

                String access_token = JWT.create()
                        .withSubject(usuarioMonitoramento.getUsuario())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                        .withIssuer(request.getRequestURI().toString())
                        .withClaim("roles", usuarioMonitoramento.getRoles().stream().map(RoleMonitoramento::getNome).collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);

                }catch(Exception e){
                response.setHeader("error", e.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("mensagem_de_erro", e.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        }else{
            throw new RuntimeException("Refresh token is missing");
        }
    }
}
