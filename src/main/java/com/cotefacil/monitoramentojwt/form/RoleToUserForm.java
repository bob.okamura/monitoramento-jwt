package com.cotefacil.monitoramentojwt.form;

import lombok.Data;

@Data
public class RoleToUserForm {

    private String usuario;
    private String rolename;
}
