package com.cotefacil.monitoramentojwt.config;

import com.cotefacil.monitoramentojwt.filter.CustomAuthenticationFilter;
import com.cotefacil.monitoramentojwt.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final PasswordEncoderSHA256Base64 passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManager());
        customAuthenticationFilter.setFilterProcessesUrl("/api/login");//Não teria necessidade de passar a url explícito, já que herda da classe "UsernamePasswordAuthenticationFilter"

        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                .antMatchers("/api/login/**", "/api/token/refresh/**", "/pedidos/**").permitAll()
                .antMatchers(HttpMethod.GET,"/api/usuarios/**").hasAnyAuthority("cotefacil", "admin")
                //.antMatchers(HttpMethod.GET,"/api/buscaporid/{id}").hasAnyAuthority("admin")
                .antMatchers(HttpMethod.POST,"/api/usuario/salvar").hasAnyAuthority("admin")
                .antMatchers(HttpMethod.DELETE,"/api/delete/**").hasAnyAuthority("admin")
                .anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter)
                .addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }
}
