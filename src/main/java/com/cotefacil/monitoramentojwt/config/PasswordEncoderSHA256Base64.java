package com.cotefacil.monitoramentojwt.config;

import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class PasswordEncoderSHA256Base64 implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        //System.out.println(charSequence);
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        String rawPassword = (String) charSequence;
        byte[] hash = digest.digest(rawPassword.getBytes(StandardCharsets.UTF_8));
        //System.out.println(Base64.getEncoder().encodeToString(hash)); //valor de retorno para verificar no banco
        return Base64.getEncoder().encodeToString(hash);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        String rawPasswordEncoded = this.encode(rawPassword);
        return this.passwordEquals(encodedPassword.toString(), rawPasswordEncoded);
    }

    private boolean passwordEquals(String expected, String actual) {
        byte[] expectedBytes = bytesUtf8(expected);
        byte[] actualBytes = bytesUtf8(actual);
        return MessageDigest.isEqual(expectedBytes, actualBytes);
    }

    private static byte[] bytesUtf8(String s) {
        return s != null ? Utf8.encode(s) : null;
    }
}

