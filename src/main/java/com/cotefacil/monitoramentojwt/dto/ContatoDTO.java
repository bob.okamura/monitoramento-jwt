package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.Contato;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class ContatoDTO implements Serializable {

    private Long id;
    private String nome;
    private String cargo;
    private String email;
    private String DDD1;
    private String fone1;
    private String DDD2;
    private String fone2;

    public ContatoDTO(Contato entity){
        id = entity.getId();
        nome = entity.getNome();
        cargo = entity.getCargo();
        email = entity.getEmail();
        DDD1 = entity.getDDD1();
        fone1 = entity.getFone1();
        DDD2 = entity.getDDD2();
        fone2 = entity.getFone2();
    }
}
