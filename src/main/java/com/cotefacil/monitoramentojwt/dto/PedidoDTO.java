package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.Cliente;
import com.cotefacil.monitoramentojwt.domain.Comprador;
import com.cotefacil.monitoramentojwt.domain.Pedido;
import lombok.Getter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
public class PedidoDTO implements Serializable {

    private Long id;
    private Long idCotacao;
    private Long idComprador;
    private Long idCliente;
    private String nomeFantasia;
    private String razaoSocial;
    private String cnpj;
    private String titulo;
    private Long idClienteCotacao;
    private String situacao;
    private String forncedorNome;
//    private String representante;


    public PedidoDTO(Pedido entity){
        id = entity.getId();
        idCotacao = entity.getCotacao().getId();
        idComprador = entity.getComprador().getId();
        idCliente = entity.getCliente().getId();
        nomeFantasia = entity.getCliente().getNomeFantasia();
        razaoSocial = entity.getCliente().getRazaoSocial();
        cnpj = entity.getCliente().getCnpj();
        titulo = entity.getCotacao().getTitulo();
        idClienteCotacao = entity.getCotacao().getId();
        situacao = entity.getSituacao().getNome();
        forncedorNome = entity.getFornecedor().getNomeFantasia();

    }
}
