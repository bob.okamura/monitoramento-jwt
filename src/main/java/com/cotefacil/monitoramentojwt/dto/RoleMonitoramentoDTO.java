package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RoleMonitoramentoDTO {

    private Long id;
    private String nome;

    public RoleMonitoramentoDTO(RoleMonitoramento entity){
        id = entity.getId();
        nome = entity.getNome();
    }

}
