package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.Comprador;
import lombok.Getter;

import java.io.Serializable;
@Getter
public class CompradorDTO implements Serializable {

    private Long idComprador;
    private Long idContato;
    private String cnpj;
    private String nomeFantasia;
    private String comprador;
    private String usuarioNome;
    private String email;
    private String ddd1;
    private String fone1;
    private String ddd2;
    private String fone2;
    private Long codCliente;
    private String usuario;
    private String senha;

    public CompradorDTO(Comprador entity){
        idComprador = entity.getId();
        idContato = entity.getContato().getId();
        cnpj = entity.getCliente().getCnpj();
        nomeFantasia = entity.getCliente().getNomeFantasia();
        comprador = entity.getContato().getNome();
        usuarioNome = entity.getUsuario().getNome();
        email = entity.getContato().getEmail();
        ddd1 = entity.getContato().getDDD1();
        fone1 = entity.getContato().getFone1();
        ddd2 = entity.getContato().getDDD2();
        fone2 = entity.getContato().getFone2();
        codCliente = entity.getCliente().getId();
        usuario = entity.getUsuario().getUsuario();
        senha = entity.getUsuario().getSenha();
    }
}
