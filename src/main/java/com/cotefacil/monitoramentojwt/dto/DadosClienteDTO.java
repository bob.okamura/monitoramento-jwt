package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.Pedido;
import lombok.Getter;

import java.io.Serializable;
@Getter
public class DadosClienteDTO implements Serializable {

    private String cnpj;
    private String nomeFantasia;
    private String comprador;
    private String usuarioNome;
    private String email;
    private String ddd1;
    private String fone1;
    private String ddd2;
    private String fone2;
    private Long codCliente;
    private String usuario;
    private String senha;

    public DadosClienteDTO(Pedido entity){
        cnpj = entity.getCliente().getCnpj();
        nomeFantasia = entity.getCliente().getNomeFantasia();
        comprador = entity.getComprador().getContato().getNome();
        usuarioNome = entity.getComprador().getUsuario().getNome();
        email = entity.getCliente().getEmail();
        ddd1 = entity.getCliente().getDdd1();
        fone1 = entity.getCliente().getFone1();
        ddd2 = entity.getCliente().getDdd2();
        fone2 = entity.getCliente().getFone2();
        codCliente = entity.getCliente().getId();
        usuario = entity.getComprador().getUsuario().getUsuario();
        senha = entity.getComprador().getUsuario().getSenha();
    }


}
