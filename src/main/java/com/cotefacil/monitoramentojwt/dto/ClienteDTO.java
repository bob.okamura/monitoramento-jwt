package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.Cliente;
import com.cotefacil.monitoramentojwt.domain.Contato;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
public class ClienteDTO implements Serializable {

    private String cnpj;
    private String nomeFantasia;
    private String email;
    private String ddd1;
    private String fone1;
    private String ddd2;
    private String fone2;

    private List<ContatoDTO> listaContatos = new ArrayList<>();

    public ClienteDTO(Cliente entity){
        cnpj = entity.getCnpj();
        nomeFantasia = entity.getNomeFantasia();
         email = entity.getEmail();
        ddd1 = entity.getDdd1();
        fone1 = entity.getFone1();
        ddd2 = entity.getDdd2();
        fone2 = entity.getFone2();
    }

    public ClienteDTO(Cliente entity, List<Contato> list){
        this(entity);
        list.forEach(contatos -> this.listaContatos.add(new ContatoDTO(contatos)));
    }
}
