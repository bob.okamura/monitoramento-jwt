package com.cotefacil.monitoramentojwt.dto;

import com.cotefacil.monitoramentojwt.domain.RoleMonitoramento;
import com.cotefacil.monitoramentojwt.domain.UsuarioMonitoramento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioMonitoramentoDTO implements Serializable {

    private Long id;
    private String usuario;
    private String password;
    private Boolean habilitado = true;
    private Date dataCadastro;

    private Collection<RoleMonitoramentoDTO> roles = new ArrayList<>();

    public UsuarioMonitoramentoDTO(UsuarioMonitoramento entity){
        id = entity.getId();
        usuario = entity.getUsuario();
        password = entity.getPassword();
        habilitado = entity.getHabilitado();
        dataCadastro = entity.getDataCadastro();
    }

    public UsuarioMonitoramentoDTO(UsuarioMonitoramento entity, Collection<RoleMonitoramento> roles){
        this(entity);
        roles.forEach(roleMonitoramento -> this.roles.add(new RoleMonitoramentoDTO(roleMonitoramento)));
    }
}
