package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "USUARIO")
@Data
public class Usuario implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_USUARIO", sequenceName = "SEQ_USUARIO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USUARIO")
    private Long id;

    @Column(name = "USUARIO")
    private String usuario;

    @Column(name = "SENHA")
    private String senha;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "BLOQUEADO")
    private boolean bloqueado = false;

    @Column(name = "ATIVO")
    private boolean ativo = true;

    @Column(name = "NUMEROACESSOS")
    private int numeroAcessos = 0;

    @Column(name = "DATACADASTRO")
    private LocalDateTime dataCadastro;

    @Column(name = "DATAATUALIZACAO")
    private LocalDateTime dataAtualizacao;

    @Column(name = "ULTIMOACESSO")
    private LocalDateTime ultimoAcesso;

    @Column(name = "ACESSOCORRENTE")
    private LocalDateTime acessoCorrente;

//    @ManyToMany(targetEntity = Configuracao.class, fetch = FetchType.LAZY)
//    @JoinTable(name = "REL_CONFIGURACAO_USUARIO",
//            joinColumns = {
//                    @JoinColumn(name = "IDUSUARIO")
//            }, inverseJoinColumns = {
//            @JoinColumn(name = "IDCONFIGURACAO")
//    })
//    private List<Configuracao> configuracoes;

//    public boolean haveAuthority(String a) {
//        return configuracoes.stream().anyMatch(configuracao -> (configuracao.getChave().equals(a) && configuracao.isAtivo()));
//    }

    @OneToOne(targetEntity = Conta.class, mappedBy = "usuario")
    private Conta conta;
}
