package com.cotefacil.monitoramentojwt.domain;

import com.cotefacil.monitoramentojwt.enums.SituacaoCotacao;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "cotacao")
@DiscriminatorColumn(name = "TIPO", discriminatorType = DiscriminatorType.CHAR, length = 1)
@DiscriminatorValue(value = "M")
@Data
public class Cotacao implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_COTACAO", sequenceName = "SEQ_COTACAO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_COTACAO")
    private Long id;

    @ManyToOne(targetEntity = Cliente.class)
    @JoinColumn(name = "IDCLIENTE")
    private Cliente cliente;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "SITUACAO")
    private SituacaoCotacao situacao = SituacaoCotacao.EM_CRIACAO;

    @Column(name = "DATAVENCIMENTO", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataVencimento;

    @Column(name = "DATACADASTRO", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataCadastro;

    //private int prazoEntrega;

    @ManyToOne(targetEntity = Comprador.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "IDCOMPRADOR")
    private Comprador comprador;

//    @ManyToMany(targetEntity = FilialCliente.class, fetch = FetchType.LAZY)
//    @JoinTable(name = "FILIALCOTACAO",
//            joinColumns = {
//                    @JoinColumn(name = "IDCOTACAO")
//            },
//            inverseJoinColumns = {
//                    @JoinColumn(name = "IDFILIALCLIENTE")
//            })
//    private List<FilialCliente> filiaisCliente;

//    @OneToMany(mappedBy = "cotacao", targetEntity = CotacaoItem.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<CotacaoItem> itens;

//    @ManyToOne(targetEntity = CondicaoPagamento.class)
//    @JoinColumn(name = "IDCONDICAOPAGAMENTO")
//    private CondicaoPagamento condicaoPagamento;

    @Column(name = "PEDIDOMANUAL")
    private Boolean pedidoManual;

    @Column(name = "MOTIVOCANCELAMENTO")
    private String motivoCancelamento;

    @OneToMany(targetEntity = Cotacao.class, fetch = FetchType.LAZY)
    @JoinTable(name = "COTACAOASSOCIADA",
            joinColumns = {
                    @JoinColumn(name = "IDCOTACAO")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "IDCOTACAOASSOCIADA")
            })
    private List<Cotacao> cotacoesAssociadas;

    @ManyToOne(targetEntity = Cotacao.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCOTACAOASSOCIADA")
    private Cotacao cotacaoAssociada;

    @OneToOne(targetEntity = Cotacao.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "IDCOTACAOREFERENCIA", referencedColumnName = "ID", insertable = true, updatable = true)
    private Cotacao cotacaoReferencia;

    @Column(name = "DATAVENCIMENTOANTERIOR")
    private LocalDateTime dataVencimentoAnterior;

//    @OneToMany(mappedBy = "cotacao", targetEntity = CotacaoRepresentante.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @Cascade(value = {org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
//    private List<CotacaoRepresentante> cotacaoRepresentantes;

    @ManyToMany(targetEntity = Pedido.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "REL_COT_PED_CAMPANHA", joinColumns = {
            @JoinColumn(name = "IDCOTACAO", referencedColumnName = "ID")}, inverseJoinColumns = {
            @JoinColumn(name = "IDPEDIDO", referencedColumnName = "ID")})
    private List<Pedido> pedidosCampanha;

    @OneToMany(mappedBy = "cotacao", targetEntity = Pedido.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Cascade(value = {org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private List<Pedido> pedidos;

    @Column(name = "STATUSESPELHORF")
    private String statusEspelhoRF = "D";

    public boolean isPedidoManual() {
        return this.pedidoManual != null && this.pedidoManual;
    }

    public boolean isVencida() {
        return dataVencimento != null && dataVencimento.isBefore(LocalDateTime.now());
    }

//    public void addCotacaoRepresentante(CotacaoRepresentante cotacaoRepresentante) {
//        if (this.cotacaoRepresentantes == null) {
//            this.cotacaoRepresentantes = new ArrayList<CotacaoRepresentante>();
//        }
//        if (!this.cotacaoRepresentantes.contains(cotacaoRepresentante)) {
//            this.cotacaoRepresentantes.add(cotacaoRepresentante);
//            cotacaoRepresentante.setCotacao(this);
//        }
//    }

//    public void addFilial(FilialCliente filialCliente) {
//        if (this.filiaisCliente == null) {
//            this.filiaisCliente = new ArrayList<>();
//        }
//
//        if (!this.listFilialContainsFilial(filialCliente)) {
//            this.filiaisCliente.add(filialCliente);
//        }
//    }

//    private boolean listFilialContainsFilial(FilialCliente filialCliente) {
//        return this.filiaisCliente.stream().filter(fc ->
//                Objects.equals(fc.getId(), filialCliente.getId())
//        ).findFirst().isPresent();
//    }

//    public void addItem(CotacaoItem item) {
//        if (itens == null) {
//            itens = new ArrayList<CotacaoItem>();
//        }
//
//        item.setCotacao(this);
//
//        if (!this.itens.contains(item)) {
//            this.itens.add(item);
//        }
//    }

}
