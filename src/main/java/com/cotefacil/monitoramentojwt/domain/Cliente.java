package com.cotefacil.monitoramentojwt.domain;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TIPO", discriminatorType = DiscriminatorType.CHAR, length = 1)
@DiscriminatorValue(value = "M")
@Table(name = "CLIENTE")
@ToString
@RequiredArgsConstructor
@Getter
public class Cliente implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CLIENTE", sequenceName = "SEQ_CLIENTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CLIENTE")
    private Long id;

    @Column(name = "CNPJ")
    private String cnpj;

    @Column(name = "RAZAOSOCIAL")
    private String razaoSocial;

    @Column(name = "NOMEFANTASIA")
    private String nomeFantasia;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "DDD")
    private String ddd1;

    @Column(name = "TELEFONE")
    private String fone1;

    @Column(name = "DDD2")
    private String ddd2;

    @Column(name = "TELEFONE2")
    private String fone2;

//    @OneToMany(mappedBy = "cliente", targetEntity = EnderecoSite.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<EnderecoSite> enderecos;

    @OneToMany(mappedBy = "cliente", targetEntity = Contato.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Contato> contatos;

    @Column(name = "OBSERVACAO")
    private String observacao;

//    @Column(name = "SITUACAO")
//    private SituacaoClienteSite situacao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATACADASTRO")
    private Date dataCadastro;

    @Column(name = "RESPOSTAOUTROS")
    private String respostaOutros;

    @Column(name = "EQUIPEINTERNA")
    private boolean soubeEquipeInterna;

    @Column(name = "PROPAGANDA")
    private boolean soubePropaganda;

    @Column(name = "INTERNET")
    private boolean soubeInternet;

    @Column(name = "SOFTWAREHOUSE")
    private boolean soubeSoftwareHouse;

    @Column(name = "FORNECEDOR")
    private boolean soubeFornecedor;

    @Column(name = "OUTROS")
    private boolean soubeOutros;

//    @ManyToOne(targetEntity = Segmento.class)
//    @JoinColumn(name = "IDSEGMENTO")
//    private Segmento segmento;

//    public void addContato(ContatoSite contatoSite) {
//        this.contatos.add(contatoSite);
//    }

//    public void addEndereco(EnderecoSite enderecoSite) {
//        this.enderecos.add(enderecoSite);
//    }

    public boolean isSoubeEquipeInterna() {
        return soubeEquipeInterna;
    }

    public boolean isSoubePropaganda() {
        return soubePropaganda;
    }

    public boolean isSoubeInternet() {
        return soubeInternet;
    }

    public boolean isSoubeSoftwareHouse() {
        return soubeSoftwareHouse;
    }

    public boolean isSoubeFornecedor() {
        return soubeFornecedor;
    }

    public boolean isSoubeOutros() {
        return soubeOutros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Cliente cliente = (Cliente) o;
        return id != null && Objects.equals(id, cliente.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
