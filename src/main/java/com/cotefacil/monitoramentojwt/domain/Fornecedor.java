package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "fornecedor")
@Data
public class Fornecedor implements Serializable {
    @Id
    @SequenceGenerator(name = "SEQ_FORNECEDOR", sequenceName = "SEQ_FORNECEDOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FORNECEDOR")
    private Long id;

    @Column(name = "CNPJ")
    private String cnpj;

    @Column(name = "NOMEFANTASIA")
    private String nomeFantasia;

    @Column(name = "RAZAOSOCIAL")
    private String razaoSocial;

    @ManyToMany(targetEntity = Representante.class, fetch = FetchType.LAZY)
    @JoinTable(name = "REPRESENTANTEFORNECEDOR",
            joinColumns = {
                    @JoinColumn(name = "IDFORNECEDOR")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "IDREPRESENTANTE")
            })
    private List<Representante> representantes;

    @Column(name = "COTACAOINTEGRADA")
    private boolean cotacaoIntegrada;

    @Column(name = "NOVOPADRAO")
    private boolean novoPadrao;

//    @ManyToOne(targetEntity = GrupoFornecedor.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
//    @JoinColumn(name = "IDGRUPOFORNECEDOR", referencedColumnName = "ID", insertable = true, updatable = true)
//    private GrupoFornecedor grupo;

}
