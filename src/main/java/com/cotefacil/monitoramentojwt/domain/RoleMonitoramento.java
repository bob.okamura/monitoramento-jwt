package com.cotefacil.monitoramentojwt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "MONIT_ROLE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleMonitoramento implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_MONIT_ROLE", sequenceName = "SEQ_MONIT_ROLE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MONIT_ROLE")
    private Long id;
    @Column(name = "NOME")
    private String nome;

    @ManyToMany(targetEntity = UsuarioMonitoramento.class, mappedBy = "roles")
    private List<UsuarioMonitoramento> usuarios;
}
