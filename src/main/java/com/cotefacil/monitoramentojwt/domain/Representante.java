package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "REPRESENTANTE")
@Data
public class Representante extends Conta implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CONTA", sequenceName = "SEQ_CONTA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONTA")
    private Long id;

    @ManyToMany(targetEntity = Fornecedor.class, mappedBy = "representantes", fetch = FetchType.LAZY)
    private List<Fornecedor> fornecedores;

//    @Column(name = "TIPORESPOSTA")
//    private TipoRespostaRepresentante tipoResposta = TipoRespostaRepresentante.MANUAL;

    @Column(name = "UTILIZADESCONTOFINANCEIROSEMST")
    private boolean utilizaDescontoFinanceiroSemST = false;

    @Column(name = "CALCULARMINIMOFATURAMENTOSEMST")
    private boolean calcularMinimoFaturamentoSemST = false;

    @Column(name = "GERARRESPPEDIDOELETRONICO")
    private boolean gerarRespostaPorPedidoEletronico = false;

    @Column(name = "RESPONDERBASEPRECO")
    private boolean responderBasePreco = false;

    @Column(name = "INTEGRACAOCRAWLER")
    private boolean integracaoCrawler = false;

    @Column(name = "BUSCARPRECOFABRICA")
    private Boolean buscarPrecoFabrica = false;

    @Column(name = "UTILIZADESCONTOFINANCEIRO")
    private boolean utilizaDescontoFinanceiro = false;

    @Column(name = "CANCELAMENTOCOTACAO")
    private boolean cancelamentoCotacao = true;

    @Column(name = "PRAZOALTERADO")
    private boolean prazoAlterado = true;

    @Column(name = "RESPOSTABASEPRECO")
    private boolean respostaBasePreco;

    @Column(name = "VERIFICARRESPOSTAELETRONICA")
    private boolean verificarRespostaEletronica;

    //CAMPO USADO PARA O REPRESENTANTE SEMI-AUTOMÁTICO
    @Column(name = "ENVIOPEDIDOELETRONICO")
    private boolean envioPedidoEletronico = false;

    @ManyToMany(targetEntity = Comprador.class, mappedBy = "representantes", fetch = FetchType.LAZY)
    private List<Comprador> compradores;

    public Boolean getBuscarPrecoFabrica() {
        if (this.buscarPrecoFabrica == null) {
            return false;
        }
        return this.buscarPrecoFabrica;
    }

}
