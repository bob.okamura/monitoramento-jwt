package com.cotefacil.monitoramentojwt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "MONIT_USUARIO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioMonitoramento implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_MONIT_USUARIO", sequenceName = "SEQ_MONIT_USUARIO_TEST", allocationSize = 1)
//    @SequenceGenerator(name = "SEQ_MONIT_USUARIO", sequenceName = "SEQ_MONIT_USUARIO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MONIT_USUARIO")
    private Long id;

    @Column(name = "USUARIO")
    private String usuario;

    @Column(name = "SENHA")
    private String password;

    @Column(name = "HABILITADO")
    private Boolean habilitado = false;

    @Column(name = "ENVIO")
    private Boolean envio = false;

    @Column(name = "SQL")
    private Boolean sql = false;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATACADASTRO")
    private Date dataCadastro;

    @ManyToMany(targetEntity = RoleMonitoramento.class, fetch = FetchType.EAGER)
    @JoinTable(name = "MONIT_REL_ROLE_USUARIO",
            joinColumns = {
                    @JoinColumn(name = "ID_USUARIO")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_ROLE")
            })
    private List<RoleMonitoramento> roles = new ArrayList<>();
}
