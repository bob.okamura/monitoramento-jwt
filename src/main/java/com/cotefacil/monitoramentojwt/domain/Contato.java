package com.cotefacil.monitoramentojwt.domain;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONTATO")
@Data
public class Contato implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CONTATO", sequenceName = "SEQ_CONTATO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONTATO")
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CARGO")
    private String cargo;

    @Column(name = "EMAIL", length = 400)
    private String email;

    @Column(name = "DDD1")
    private String DDD1;

    @Column(name = "FONE1")
    private String fone1;

    @Column(name = "DDD2")
    private String DDD2;

    @Column(name = "FONE2")
    private String fone2;

    @Column(name = "CONTRATOACEITO")
    private boolean contratoAceito;

    @ManyToOne(targetEntity = Cliente.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCLIENTE", referencedColumnName = "ID", insertable = true, updatable = true)
    private Cliente cliente;

    public Contato() {
    }
}
