package com.cotefacil.monitoramentojwt.domain;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONTA")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class Conta implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CONTA", sequenceName = "SEQ_CONTA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONTA")
    private Long id;

    @OneToOne(targetEntity = Usuario.class)
    @JoinColumn(name = "IDUSUARIO")
    private Usuario usuario;

    @OneToOne(targetEntity = Contato.class)
    @JoinColumn(name = "IDCONTATO")
    private Contato contato;
}
