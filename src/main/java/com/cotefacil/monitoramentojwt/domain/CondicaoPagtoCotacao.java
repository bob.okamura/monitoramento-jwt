package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONDICAOPAGAMENTO")
@Data
public class CondicaoPagtoCotacao implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CONDICAOPAGAMENTO", sequenceName = "SEQ_CONDICAOPAGAMENTO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONDICAOPAGAMENTO")
    private Long id;

    @Column(name = "DESCRICAO")
    private String descricao;

//    private SegmentoCondicaoPagamento segmento;
//
//    private TipoCondicaoPagamento tipo;

    @ManyToOne(targetEntity = Cliente.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCLIENTE", referencedColumnName = "ID", insertable = true, updatable = true)
    private Cliente cliente;

}
