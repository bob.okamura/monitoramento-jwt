package com.cotefacil.monitoramentojwt.domain;
import com.cotefacil.monitoramentojwt.enums.SituacaoPedido;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PEDIDO")
@Data
public class Pedido implements Serializable {

    @Id
    @Access(AccessType.PROPERTY)
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_PEDIDO", sequenceName = "SEQ_PEDIDO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PEDIDO")
    private Long id;

    @ManyToOne(targetEntity = Comprador.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDCOMPRADOR")
    private Comprador comprador;

    @ManyToOne(targetEntity = Cliente.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDCLIENTE")
    private Cliente cliente;

    @ManyToOne(targetEntity = Cotacao.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDCOTACAO")
    private Cotacao cotacao;

    @ManyToOne(targetEntity = Representante.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDREPRESENTANTE")
    private Representante representante;

    @ManyToOne(targetEntity = Fornecedor.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDFORNECEDOR")
    private Fornecedor fornecedor;

    @OneToOne(targetEntity = PedidoFaturado.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPEDIDOFATURADO", referencedColumnName = "ID", insertable = true, updatable = true)
    private PedidoFaturado pedidoFaturado;

    @Column(name = "VALORTOTAL")
    private float valorTotal;

    @Column(name = "CODPEDIDOINTERNO")
    private String codPedidoInterno;

    @Column(name = "CODCONDCOMERCIAL")
    private String codCondComercial;

    @Column(name = "PRAZOENTREGA")
    private int prazoEntrega;

    @Column(name = "CODPEDIDO")
    private String codPedido;

    @Column(name = "DATACADASTRO", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataCadastro;

    @Column(name = "DATAENVIO", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataEnvio;

    @Column(name = "SITUACAO")
    private SituacaoPedido situacao = SituacaoPedido.EM_CRIACAO;

    @OneToMany(targetEntity = PedidoItem.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "pedido")
    @Cascade(value = {org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private List<PedidoItem> itens;

    @Column(name = "TEMSUBPEDIDOS")
    private boolean temSubPedidos;

    @Column(name = "ALERTAENVIADO")
    private boolean alertaEnviado;

    @Column(name = "OBSERVACAO")
    private String observacao;

    @Column(name = "CODPEDIDOFORNECEDOR")
    private String codPedidoFornecedor;

    @Column(name = "VALORECONOMIZADO")
    private Float valorEconomizado;

    @Transient
    private float valorTotalCusto;

    @Column(name = "STATUSESB")
    private String statusEsb = "D";

    @Column(name = "STATUSESBPE")
    private String statusEsbPE = "D";

    @Column(name = "DATACAPTURA", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataCaptura;

    @Column(name = "QUANTIDADEMINIMAPEDIDO")
    private Integer qtdeMinimaPedido;

    @Column(name = "VALORTOTALPEDIDORIGINAL")
    private Float valorTotalPedidoOriginal = 0f;

    @Column(name = "VALORTOTALSEMST")
    private Float valorTotalSemST = 0f;

    @ManyToOne(targetEntity = Campanha.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCAMPANHA")
    private Campanha campanha;

    @Column(name = "PRAZOPAGAMENTO")
    private Integer prazoPagamento;

    @Column(name = "PRAZOTITULO")
    private String prazoTitulo;

    @Column(name = "VALIDADE", columnDefinition = "TIMESTAMP")
    private LocalDateTime validade;

    @Column(name = "FATURAMENTOMINIMO")
    private Float faturamentoMinimo;

//    @OneToOne(targetEntity = CondicaoPagtoCotacao.class)
//    @JoinColumn(name = "IDCONDICAOPAGAMENTOCOT", referencedColumnName = "ID", insertable = true, updatable = true)
//    private CondicaoPagtoCotacao condicaoPagamentoCotacao;

//    @Column(name = "CODIGOCONDICAOPAGAMENTO")
//    private String codigoCondicaoPagamento;

    @Transient
    private boolean respostaUnica;

//    public Pedido() {
//        this.dataCadastro = LocalDateTime.now();
//        this.itens = new ArrayList<>();
//        super.setVersion(0);
//    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.cliente);
        hash = 97 * hash + Objects.hashCode(this.cotacao);
        hash = 97 * hash + Objects.hashCode(this.representante);
        hash = 97 * hash + Objects.hashCode(this.fornecedor);
        hash = 97 * hash + Objects.hashCode(this.campanha);
        hash = 97 * hash + Objects.hashCode(this.prazoPagamento);
        hash = 97 * hash + Objects.hashCode(this.prazoTitulo);
//        hash = 97 * hash + Objects.hashCode(this.codigoCondicaoPagamento);
//        hash = 97 * hash + Objects.hashCode(this.condicaoPagamentoCotacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (!(this.cliente == null ? other.cliente == null : this.cliente.getId().equals(other.cliente.getId()))) {
            return false;
        }
        if (!(this.cotacao == null ? other.cotacao == null : this.cotacao.getId().equals(other.cotacao.getId()))) {
            return false;
        }
        if (!(this.representante == null ? other.representante == null : this.representante.getId().equals(other.representante.getId()))) {
            return false;
        }
        if (!(this.fornecedor == null ? other.fornecedor == null : this.fornecedor.getId().equals(other.fornecedor.getId()))) {
            return false;
        }
        if (!(this.campanha == null ? other.campanha == null : this.campanha.getId().equals(other.campanha.getId()))) {
            return false;
        }
        if (!Objects.equals(this.prazoPagamento, other.prazoPagamento)) {
            return false;
        }
        if (!Objects.equals(this.prazoTitulo, other.prazoTitulo)) {
            return false;
        }
//        if (!Objects.equals(this.codigoCondicaoPagamento, other.codigoCondicaoPagamento)) {
//            return false;
//        }
//        if (!(this.condicaoPagamentoCotacao == null ? other.condicaoPagamentoCotacao == null : this.condicaoPagamentoCotacao.equals(other.condicaoPagamentoCotacao))) {
//            return false;
//        }
        return true;
    }

    public void recalcularPedido() {
        float total = 0;
        float totalCusto = 0;
        float totalSemST = 0;
        for (PedidoItem i : getItens()) {
            if (i.getSemAnalisePreco() != null && i.getSemAnalisePreco()) {
                total += i.getQuantidadePedida() * i.getValorSemST();
                totalCusto += i.getQuantidadePedida() * i.getValorLiquido();
            } else {
                total += i.getQuantidadePedida() * i.getValorUnitario();
                totalCusto += i.getQuantidadePedida() * i.getValorCusto();
            }
            totalSemST += i.getValorSemST() != null ? i.getQuantidadePedida() * i.getValorSemST() : 0;
        }
        valorTotal = total;
        valorTotalCusto = totalCusto;
        valorTotalSemST = totalSemST;
    }
}
