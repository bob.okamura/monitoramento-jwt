package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PEDIDOFATURADO")
@Data
public class PedidoFaturado implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_PEDIDOFATURADO", sequenceName = "SEQ_PEDIDOFATURADO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PEDIDOFATURADO")
    private Long id;

//    @OneToMany(targetEntity = PedidoFaturadoItem.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JoinColumn(name = "IDPEDIDOFATURADO", referencedColumnName = "ID", insertable = true, updatable = true)
//    @Cascade(value = {org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
//    private Collection<PedidoFaturadoItem> itens;

    @Column(name = "PERCENTUALFATURADOPRODUTOS", precision = 2)
    private Float percentualFaturadoProdutos;

}
