package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "CAMPANHA")
@Data
public class Campanha implements Serializable {
    @Id
    @Access(AccessType.PROPERTY)
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_CAMPANHA", sequenceName = "SEQ_CAMPANHA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CAMPANHA")
    private Long id;

//    @OneToMany(targetEntity = CampanhaItem.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "campanha")
//    private List<CampanhaItem> itens;
//
//    @OneToMany(targetEntity = CampanhaCliente.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "campanha")
//    private List<CampanhaCliente> campanhasCliente;
//
//    @OneToMany(targetEntity = CampanhaFornecedor.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "campanha")
//    private List<CampanhaFornecedor> campanhasFornecedor;

    @Column(name = "IDENTIFICACAO")
    private String identificacao;

    @Column(name = "PATROCINADOR")
    private String patrocinador;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "IDENTIFICACAODISTRIBUIDOR")
    private String identificacaoDistribuidor;

    @Column(name = "IDENTIFICACAOOFERTANTE")
    private String identificacaoOfertante;

    @Column(name = "ELETRONICO")
    private boolean eletronico;

    @Column(name = "PERIODOINICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoInicio;

    @Column(name = "PERIODOFIM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoFim;

    @Column(name = "OBSERVACAO")
    private String observacao;

    @Column(name = "ATIVO")
    private boolean ativo;

    @Column(name = "VALORMINIMOPEDIDO")
    private Float valorMinimoPedido;

    @Column(name = "DATAATUALIZACAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAtualizacao;

    @Column(name = "RESUMOPRODUTOS")
    private String resumoProdutos;

    @Column(name = "RESUMOFABRICANTES")
    private String resumoFabricantes;

    @Column(name = "QUANTIDADEMINIMAPEDIDO")
    private Integer qtdeMinimaPedido;

    @Column(name = "CODINTERNO")
    private Long codInterno;

    @Column(name = "PRAZO")
    private Integer prazo;

    @Column(name = "PRAZOTITULO")
    private String prazoTitulo;

    private String codigoCondicaoPagamento;

    @Transient
    private Double valorEconomizado;

//    @ManyToOne(targetEntity = Estado.class)
//    @JoinColumn(name = "IDESTADO", referencedColumnName = "ID", insertable = true, updatable = true)
//    private Estado estado;

//    @OneToOne(targetEntity = CampanhaOrigem.class)
//    @JoinColumn(name = "IDCAMPANHAORIGEM", referencedColumnName = "ID", insertable = true, updatable = true)
//    private CampanhaOrigem campanhaOrigem;

    @Override
    public int hashCode() {
        int hash = 11;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if ((getClass() != obj.getClass()) && !(obj instanceof Campanha)) {
            return false;
        }
        final Campanha other = (Campanha) obj;
        if (this.id != other.getId() && (this.id == null || !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

}
