package com.cotefacil.monitoramentojwt.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PEDIDOITEM")
@Data
public class PedidoItem implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "SEQ_PEDIDOITEM", sequenceName = "SEQ_PEDIDOITEM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PEDIDOITEM")
    private Long id;

    @ManyToOne(targetEntity = Pedido.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "IDPEDIDO", referencedColumnName = "ID", insertable = true, updatable = true)
    private Pedido pedido;

//    @ManyToOne(targetEntity = Produto.class)
//    @JoinColumn(name = "IDPRODUTO")
//    private Produto produto;

    @Column(name = "QUANTIDADE")
    private int quantidade;

    @Column(name = "QUANTIDADEPEDIDA")
    private int quantidadePedida;

    @Column(name = "QUANTIDADECOTADA")
    private int quantidadeCotada;

    @Column(name = "QUANTIDADERESPONDIDA")
    private int quantidadeRespondida;

    @Column(name = "QUANTIDADEFATURADA")
    private int quantidadeFaturada;

//    @Column(name = "TIPOEMBALAGEM")
//    private TipoEmbalagem tipoEmbalagem;

//    @Column(name = "ORIGEMRESPOSTA")
//    private OrigemResposta origemResposta;

    @Column(name = "QUANTIDADEEMBALAGEM")
    private int quantidadeEmbalagem;

    @Column(name = "VALORUNITARIO")
    private float valorUnitario;

    @Column(name = "VALORLIQUIDO")
    private Float valorLiquido;

    @Column(name = "VALORCUSTO")
    private float valorCusto;

    @Column(name = "VALORREFERENCIA")
    private float valorReferencia;

    @Column(name = "QUANTIDADEDIASFALTA")
    private int quantidadeDiasFalta;

    @Column(name = "DESCONTO")
    private float desconto;

    @Column(name = "SITUACAOFECHAMENTO")
    private int situacaoFechamento;

//    @ManyToOne(targetEntity = ProdutoFornecedor.class, fetch = FetchType.LAZY)
//    @JoinColumn(name = "IDPRODUTOFORNECEDOR")
//    private ProdutoFornecedor produtoFornecedor;

    @Column(name = "SUBPEDIDO")
    private long subPedido;

//    @Embedded
//    private VOProdutoCliente produtoCliente;

    @Column(name = "DESCONTOINFORMADO")
    private float descontoInformado;

    @Column(name = "QUANTIDADEALTERADA")
    private boolean quantidadeAlterada;

    @Column(name = "DESCONTOFINANCEIRO", precision = 2)
    private Float descontoFinanceiro;

    @Column(name = "VALORCOMST", precision = 2)
    private Float valorComST;

    @Column(name = "JUSTIFICATIVA")
    private String justificativa;

    @Column(name = "VALORUNITARIOORIGINAL")
    private float valorUnitarioOriginal;

    @Column(name = "QUANTIDADEORIGINAL")
    private int quantidadeOriginal;

//    @Column(name = "TIPOEMBALAGEMORIGINAL")
//    private TipoEmbalagem tipoEmbalagemOriginal;

    @Column(name = "QUANTIDADEEMBALAGEMORIGINAL")
    private int quantidadeEmbalagemOriginal;

    @Column(name = "FEITOCONVERSAO")
    private boolean feitoConversao = false;

    @Column(name = "CODFATURAMENTOPROMOCAO")
    private String codigoFaturamentoPromocao;

    @Column(name = "PRAZOPAGAMENTOCAMPANHA")
    private Integer prazoPagamentoCampanha;

    @Column(name = "OBRIGATORIO")
    private Boolean obrigatorio;

    @Column(name = "FIXO")
    private Boolean fixo;

    @Column(name = "QUANTIDADEMINIMA")
    private Integer qtdeMinima;

    @Column(name = "NAOCLASSIFICADO")
    private Boolean naoClassificado;

    @Column(name = "QTDECOMPRADAPROMOCAO")
    private Integer qtdeCompradaPromocao;

    @Column(name = "DESCONTOADICIONALOPORUNIDADE")
    private Float descontoAdicionalOportunidade;

    @Column(name = "VALORUNITARIODESCOPORUNIDADE")
    private Float valorUnitarioDescOportunidade;

    @Column(name = "VALORBOLETO")
    private Float valorBoleto;

    @Column(name = "PRECOFABRICA")
    private Float precoFabrica;

    @Column(name = "ESTOQUEDISPONIVEL")
    private Integer estoqueDisponivel;

    @Column(name = "MEDIADESAIDADIA")
    private Integer mediaDeSaidaDia;

    @Column(name = "CURVADOPRODUTO")
    private String curvaDoProduto;

    @Column(name = "VALORSEMST")
    private Float valorSemST;

    @Column(name = "BLOQUEIOPRODUTOVALORCUSTO")
    private boolean bloqueioProdutoValorCusto = false;

    @Column(name = "QTDEEMBPE")
    private Long qtdeEmbPE;

    @Column(name = "UNIDMEDPE")
    private String unidMedPE;

    @Column(name = "SEMANALISEPRECO")
    private Boolean semAnalisePreco;

    @Column(name = "CODPRODUTOCLIENTEPE")
    private Long codProdutoClientePE;

    @Column(name = "GRUPOPRODUTO")
    private Boolean grupoProduto;

//    @OneToOne(targetEntity = PedidoFaturadoItem.class, fetch = FetchType.LAZY)
//    @JoinColumn(name = "IDPEDIDOFATURADOITEM", referencedColumnName = "ID", insertable = true, updatable = true)
//    private PedidoFaturadoItem pedidoFaturadoItem;

//    @OneToOne(targetEntity = PedidoItemSugerido.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "IDPEDIDOITEMSUGERIDO", referencedColumnName = "ID", insertable = true, updatable = true)
//    @Cascade(value = {org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
//    private PedidoItemSugerido pedidoItemSugerido;

//    @ManyToOne(targetEntity = DivergenciaDesconto.class)
//    @JoinColumn(name = "IDDIVERGENCIADESCONTO", referencedColumnName = "ID", insertable = true, updatable = true)
//    private DivergenciaDesconto divergenciaDesconto;

//    @ManyToOne(targetEntity = AcordoCompra.class)
//    @JoinColumn(name = "IDACORDOCOMPRA")
//    private AcordoCompra acordoCompra;

//    @ManyToOne(targetEntity = PedidoManualItemResposta.class, fetch = FetchType.LAZY)
//    @JoinColumn(name = "IDPEDIDOMANUALITEMRESPOSTA")
//    private PedidoManualItemResposta pedidoManualItemResposta;

    @Column(name = "OPORTUNIDADE")
    private boolean oportunidade;

    @Column(name = "QUANTIDADEFIXA")
    private boolean quantidadeFixa;

//    public PedidoItem() {
//        super.setVersion(0);
//    }

    public boolean isGrupoProduto() {
        return this.grupoProduto != null && this.grupoProduto;
    }

}
