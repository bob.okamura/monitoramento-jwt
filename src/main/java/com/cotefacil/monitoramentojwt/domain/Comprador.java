package com.cotefacil.monitoramentojwt.domain;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "COMPRADOR")
@Data
public class Comprador extends Conta implements Serializable {

    @ManyToOne(targetEntity = Cliente.class)
    @JoinColumn(name = "IDCLIENTE")
    private Cliente cliente;

    @Column(name = "ANALISEMAIORDESCONTO")
    private Boolean analiseMaiorDesconto;

    @ManyToMany(targetEntity = Representante.class, fetch = FetchType.LAZY)
    @JoinTable(name = "REPRESENTANTECOMPRADOR",
            joinColumns = {
                    @JoinColumn(name = "IDCOMPRADOR")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "IDREPRESENTANTE")
            })
    private List<Representante> representantes;

//    @OneToMany(mappedBy = "comprador", targetEntity = FornecedorComprador.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<FornecedorComprador> fornecedores;

//    @ManyToOne(targetEntity = FilialCliente.class, fetch = FetchType.LAZY)
//    @JoinColumn(name = "IDFILIAL")
//    private FilialCliente filial;

//    public Comprador() {
//        super();
//        fornecedores = new ArrayList<>();
//    }
}
